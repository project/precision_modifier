<?php

namespace Drupal\precision_modifier\util;

/**
 * A utility class that extracts the content type from the url.
 *
 * @package Drupal\precision_modifier\Util
 */
class EntityFromUri {
  /**
   * Gets the entity type from the URI.
   *
   * @return string id of the bundle or content type
   */
  public static function currentUriEntity(): string
  {
    $currentUri = \Drupal::request()->getRequestUri();
    return explode('/', $currentUri)[5];
  }
}
