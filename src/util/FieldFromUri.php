<?php

namespace Drupal\precision_modifier\util;

/**
 * Utility class the provides a way to get the field name from the URI.
 */
class FieldFromUri {

  /**
   * Gets the current field for a URI.
   *
   * @return mixed|string
   */
  public static function currentUriField(){
    $currentUri = \Drupal::request()->getRequestUri();
    return explode('.', explode('/', $currentUri)[7])[2];
  }
}
